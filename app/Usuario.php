<?php
/**
 * PHP Version 7.2.10
 * 
 * @category Model
 * @package  App
 * @author   Carlos <carlosflres095@gmail.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
 * @link     https://yoursite.com
 */
namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Auth\Authenticatable;
use App\UsuarioController;
use App\Notifications\ResetPassNotification;
/**
 * Asd
 * 
 * @category Model
 * @package  App
 * @author   Carlos <carlosflres095@gmail.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
 * @link     https://yoursite.com
 */
class Usuario extends Model implements AuthenticatableContract 
{
    use Authenticatable;
    protected $fillable = ['nombre', 'email', 'password', 'banco', 'cuenta', 'clabe', 
    'departamento', 'orden_servicio', 'estatus', 'empresas_id', 'categorias_id'];
    protected $table = 'usuarios';
    protected $primarykey = 'id';
    protected $attributes = [
                'banco' => "", 
                'cuenta' => "", 
                'clabe' => "",
            ];

    /**
     * Atributos para restablecimiento de contraseña
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Relacion usuario empresa
     * 
     * @return .
     */
    public function empresa()
    {
        return $this->belongsTo(Empresa::class, 'empresas_id');
    }

    /**
     * Relacion usuario categoria
     * 
     * @return .
     */
    public function categoria()
    {
        return $this->belongsTo(Categoria::class, 'categorias_id');
    }

    /**
     * Sobreescribe el método de la clase ResetPassword
     * 
     * @param  string  $token
     * @return void
     */

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassNotification($token));
    }
}
