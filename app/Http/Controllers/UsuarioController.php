<?php
/**
 * PHP Version 7.2.10
 * 
 * @category Controller
 * @package  App\Http\Controllers
 * @author   Carlos <carlosflres095@gmail.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
 * @link     https://yoursite.com
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Usuario;
use App\Empresa;
use Hash;

/**
 * Usuario Controller
 * 
 * @category Controller
 * @package  App\Http\Controllers
 * @author   Carlos <carlosflres095@gmail.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
 * @link     https://yoursite.com
 */
class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = Usuario::all();
        $empresas = DB::table('empresas')->get();

        return view('nuevousuario.index', compact('usuarios', 'empresas'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('nuevousuario.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request The request object
     * 
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $usuario = new Usuario;

        $v = \Validator::make(
            $request->all(), [

            'nombre'=> 'required|string',
            'email' => 'required|email|unique:usuarios',
            //'banco' => 'required',
            //'cuenta' => 'required|numeric|digits_between:10,34',
            //'clabe' => 'required|numeric|digits:18',
            'departamento' => 'required|integer',
            'orden_servicio' => 'required|integer|max:2147483647',
            'estatus' => 'required',
            'empresas_id' => 'required|integer',
            'categorias_id' => 'required|integer',
            'password' => 'required'
            
            ]
        );

        if ($v->fails()) {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }

        //$usuario->create($request->all());
        $usuario->nombre = $request->input('nombre');
        $usuario->email = $request->input('email');
        $usuario->departamento = $request->input('departamento');
        $usuario->orden_servicio = $request->input('orden_servicio');
        $usuario->estatus = $request->input('estatus');
        $usuario->empresas_id = $request->input('empresas_id');
        $usuario->categorias_id = $request->input('categorias_id');
        $usuario->password = Hash::make($request->input('password'));
        $usuario->save();
        $usuarios = Usuario::all();

        return redirect('/usuarios/nuevo')
                ->with('success', 'Usuario creado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param id $id id
     * 
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param id $id id
     * 
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request The request object
     * @param id      $id      id
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param id $id id
     * 
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Retorna la vista para recuperar la contraseña
     * 
     * @return \Illuminate\Http\Response
     */
    public function requestPasswordReset()
    {
        return view('nuevousuario.passwords.recuperacion');
    }
}
