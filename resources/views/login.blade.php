@extends('template2')

@section('title','Iniciar Sesión')

@section('content')
<div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
  <div class="col-lg-6">
    <div class="p-5">
      <div class="text-center">
        <h1 class="h4 text-gray-900 mb-4">¡Bienvenido de vuelta!</h1>
      </div>
    <form class="user" method="POST" action="{{url('login')}}">
      @csrf
        <div class="form-group">
          <input type="email" name="email" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Correo electrónico...">
          @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
          @endif
        </div>
        <div class="form-group">
          <input type="password" name="password"class="form-control form-control-user" id="exampleInputPassword" placeholder="Contraseña">
        @if ($errors->has('password'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
        </div>
        <button class="btn btn-primary btn-user btn-block">
          Iniciar sesión
        </button>
      </form>
      <hr>
      <div class="text-center">
        <a class="small" href="{{url('/recuperar')}}">Olvidaste tu contraseña?</a>
      </div>
    </div>
  </div>
@endsection