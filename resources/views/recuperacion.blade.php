@extends('template2')

@section('title','Recuperar Contraseña')

@section('header','Recuperar Contraseña')

@section('content')            
<div class="col-lg-6 d-none d-lg-block bg-password-image"></div>
<div class="col-lg-6">
  <div class="p-5">
    <div class="text-center">
      <h1 class="h4 text-gray-900 mb-2">¿Olvidaste tu contraseña?</h1>
      <p>Lo entendemos, a veces pasa...</p>
      <p  class="mb-4">Ingresa tu <strong class="text-primary">Correo Electrónico</strong> y nosotros te enviaremos un enlace para que puedas reiniciar tu contraseña!</p>
    </div>
    <form class="user">
      <div class="form-group">
        <input type="email" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Ingresa tu Correo electrónico...">
      </div>
      <a href="#" class="btn btn-primary btn-user btn-block">
        Reiniciar contraseña
      </a>
    </form>
    <hr>
    <div class="text-center">
      <a class="small" href="{{url('/')}}">¿Ya tienes una cuenta? Inicia sesión!</a>
    </div>
  </div>
</div>
@endsection