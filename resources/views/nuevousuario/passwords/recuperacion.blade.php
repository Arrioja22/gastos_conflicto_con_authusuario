@extends('template2')

@section('title','Recuperar Contraseña')

@section('header','Recuperar Contraseña')

@section('content')
<div class="row justify-content-center mb-7">
<div class="col-lg-5 col-md-8">
    <div class="card bg-secondary shadow border-0">
    <div class="card-body px-lg-5 ">
        <div class="text-center text-muted mb-4">
            <h3 class="mb-0">¿Olvidaste tu contraseña?</h3>
            <p class="mb-0 text-muted">Ingresa la <strong>dirección de correo electrónico</strong> que usaste para registrarte.
                Te enviaremos un mensaje con un enlace para restablecer tu contraseña.
            </p>
        </div>
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        <form role="form" action="{{ route('send_email') }}" method="POST">
            @csrf
            <div class="form-group mb-3">
                <div class="input-group input-group-alternative">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                    </div>
                    <input class="form-control" placeholder="Correo electrónico" type="email" name="email" required>
                </div>
            </div>
            @if ($errors->has('email'))
                <span class="invalid-feedback d-block" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
            <div class="text-center">
                <input class="btn btn-info my-2" type="submit" value="Enviar">
            </div>
        </form>
    </div>
    </div>
</div>
</div>
@endsection