@extends('template')

@section('title','Crear usuario')

@section('header')
@if (session('success'))
    <div class="sufee-alert alert with-close alert-success alert-dismissible fade show" role="alert">
        {{ session()->get('success') }}	
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
<a href="{{url('/usuarios')}}" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left"></i> Regresar</a>

@endsection

@section('content')
<div class="row">
    <div class="col-lg-8 mx-auto">
        <div class="card shadow">
            <div class="card-header bg-info text-center">
                <h1 class="text-white mb-0">Nuevo usuario</h1>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col">

                        <form action="{{ url('/usuarios/nuevo/crear')}}" method="post">
                        {!! csrf_field() !!} 

                            <div class="form-group">
                            <label class="text-info" for="nombre">Nombre:</label>
                                <input type="text" class="form-control" 
                                        placeholder="Nombre" name="nombre"
                                        pattern="[A-Z a-z]+" required>
                            </div>

                            <div class="form-group">
                            <label class="text-info" for="email">Correo electrónico:</label>
                                <input type="email" class="form-control"
                                        name="email" placeholder="Correo electrónico"
                                        required>
                            </div>
                        <!--
                            <div class="form-group">
                            <label class="text-info" for="banco">Información Bancaria:</label>
                                <input type="text" class="form-control"
                                        name="banco" placeholder="Banco"  
                                        required>
                            </div>
                            
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <input type="number" class="form-control" 
                                                name="cuenta" 
                                                placeholder="Cuenta" required>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <input type="number" class="form-control" 
                                                name="clabe" pattern="[0-9]{18}" 
                                                placeholder="Clabe (18 digitos)" required>
                                    </div>
                                </div>
                            </div>
                            !-->
                            <div class="form-group">
                            <label class="text-info" for="departamento">Departamento:</label>
                                <select name="departamento" id="departamentos" class="form-control" required>
                                <option value="">Selecciona un departamento </option>
                                <option value="1">Ventas</option>
                                <option value="2">Sistemas</option>
                                </select>
                            </div>

                            <div class="form-group">
                            <label class="text-info" for="orden_servicio">Orden de Servicio:</label>
                                <input type="number" class="form-control" 
                                        name="orden_servicio" 
                                        placeholder="Ingrese Numero de Orden" required>
                            </div>

                            <div class="form-group">
                            <label class="text-info" for="estatus">Estatus:</label><br>
                            <input type="radio" name="estatus" value="Activo" checked>Activo
                            <input type="radio" name="estatus" value="Apagado">Apagado
                            </div>
                            
                            <div class="form-group">
                            <label class="text-info" for="empresa">Empresa:</label>
                                <select name="empresas_id" id="empresas_id" 
                                        class="form-control" placeholder="Empresa" required>
                                        <option value="">Selecciona una Empresa </option>
                                        @foreach ($empresas as $emp)
                                            <option value="{{$emp->id}}">{{ $emp->nombre }} </option>
                                        @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                            <label class="text-info" for="categorias_id">Categoría:</label>
                                <select name="categorias_id" id="categorias_id" class="form-control" placeholder="Categoría" required>
                                    <option value="">Selecciona Categoria</option>
                                    <option value="1">Supervisor</option>
                                    <option value="2">Empleado</option>
                                </select>
                            </div>

                            <div class="form-group">
                            <label class="text-info" for="password">Contraseña:</label>
                                <input type="password" name="password"
                                        class="form-control" placeholder="Contraseña" 
                                        required>
                            </div>

                            <div class="form-group mb-0">
                                <button type="submit" class="btn btn-block btn-info">Crear</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection