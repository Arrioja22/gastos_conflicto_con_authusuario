@extends('template')

@section('title','Lista de usuarios')

@section('header')
<a href="{{url('/usuarios')}}" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left"></i> Regresar</a>
@endsection

@section('content')
<div class="row mt-4">
    <div class="col-lg mx-auto">
        <div class="card shadow border-bottom-primary">
            <div class="card-body table-responsive">
                <table id="usuarios" class="table align-items-center table-hover table-borderless table-flush">
                    <thead class="thead-light">
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Empresa</th>
                        <th>Departamento</th>
                        <th>Categoría</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Isaac Newton</td>
                            <td>Isaac@email.com</td>
                            <td>Google</td>
                            <td>Marketing</td>
                            <td class="text-success">Empleado</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Nickola Tesla</td>
                            <td>Nickola@email.com</td>
                            <td>Tesla Motors</td>
                            <td>Engineering</td>
                            <td class="text-danger">Supervisor</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Henry Ford</td>
                            <td>HFord@email.com</td>
                            <td>Ford</td>
                            <td>Ejecutivo</td>
                            <td class="text-danger">Supervisor</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Linus Torvalds</td>
                            <td>Ubuntu@email.com</td>
                            <td>Linux</td>
                            <td>Developing</td>
                            <td class="text-success">Empleado</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>Steve Wozniak</td>
                            <td>SteWoz@email.com</td>
                            <td>Apple</td>
                            <td>Enginering</td>
                            <td class="text-success">Empleado</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection