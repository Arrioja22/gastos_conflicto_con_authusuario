@extends('template')

@section('title','Permisos de Usuario')

@section('header')
<a href="{{url('/usuarios')}}" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left"></i> Regresar</a>
@endsection

@section('content')
<div class="row mt-4">
    <div class="col-lg mx-auto">
        <div class="card border-bottom-success shadow">
            <div class="card-body table-responsive">
                <table id="usuarios" class="table align-items-center table-hover table-borderless table-flush">
                    <thead class="thead-light">
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Empresa</th>
                        <th>Categoría</th>
                        <th>Permisos</th>
                        <th></th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Isaac Newton</td>
                            <td>Google</td>
                            <td class="text-success">Empleado</td>
                            <td><span class="badge badge-pill badge-info">solicitar presupuestos</span><span class="badge badge-pill badge-info">Reportar gastos</span></td>
                            <td><a href="{{url('usuarios/permisos/id')}}" class="btn btn-success btn-sm">Editar permisos</a></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Nickola Tesla</td>
                            <td>Tesla Motors</td>
                            <td class="text-danger">Supervisor</td>
                            <td><span class="badge badge-pill badge-info">Permiso 1</span><span class="badge badge-pill badge-info">Permiso 2</span><span class="badge badge-pill badge-info">Permiso 3</span></td>
                            <td><a href="{{url('usuarios/permisos/id')}}" class="btn btn-success btn-sm">Editar permisos</a></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Henry Ford</td>
                            <td>Ford</td>
                            <td class="text-danger">Supervisor</td>
                            <td><span class="badge badge-pill badge-info">Permiso 1</span><span class="badge badge-pill badge-info">Permiso 2</span><span class="badge badge-pill badge-info">Permiso 3</span></td>
                            <td><a href="{{url('usuarios/permisos/id')}}" class="btn btn-success btn-sm">Editar permisos</a></td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Linus Torvalds</td>
                            <td>Linux</td>
                            <td class="text-success">Empleado</td>
                            <td><span class="badge badge-pill badge-info">Permiso 1</span></td>
                            <td><a href="{{url('usuarios/permisos/id')}}" class="btn btn-success btn-sm">Editar permisos</a></td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>Steve Wozniak</td>
                            <td>Apple</td>
                            <td class="text-success">Empleado</td>
                            <td><span class="badge badge-pill badge-info">Permiso 1</span><span class="badge badge-pill badge-info">Permiso 2</span></td>
                            <td><a href="{{url('usuarios/permisos/id')}}" class="btn btn-success btn-sm">Editar permisos</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
