@extends('template')

@section('title','Actualizar usuarios')

@section('header')
<a href="{{url('/usuarios')}}" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left"></i> Regresar</a>
@endsection

@section('content')
<div class="row mt-4">
    <div class="col-lg">
        <div class="card border-bottom-info shadow">
            <div class="card-body table-responsive">
                <table id="usuarios" class="table align-items-center table-hover table-borderless">
                    <thead class="thead-light">
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Empresa</th>
                        <th>Departamento</th>
                        <th>Categoría</th>
                        <th></th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Isaac Newton</td>
                            <td>Isaac@email.com</td>
                            <td>Google</td>
                            <td>Marketing</td>
                            <td class="text-success">Empleado</td>
                            <td><a href="{{url('usuarios/actualizar/id')}}" class="btn btn-info btn-sm">Actualizar</a></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Nickola Tesla</td>
                            <td>Nickola@email.com</td>
                            <td>Tesla Motors</td>
                            <td>Engineering</td>
                            <td class="text-danger">Supervisor</td>
                            <td><a href="{{url('usuarios/actualizar/id')}}" class="btn btn-info btn-sm">Actualizar</a></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Henry Ford</td>
                            <td>HFord@email.com</td>
                            <td>Ford</td>
                            <td>Ejecutivo</td>
                            <td class="text-danger">Supervisor</td>
                            <td><a href="{{url('usuarios/actualizar/id')}}" class="btn btn-info btn-sm">Actualizar</a></td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Linus Torvalds</td>
                            <td>Ubuntu@email.com</td>
                            <td>Linux</td>
                            <td>Developing</td>
                            <td class="text-success">Empleado</td>
                            <td><a href="{{url('usuarios/actualizar/id')}}" class="btn btn-info btn-sm">Actualizar</a></td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>Steve Wozniak</td>
                            <td>SteWoz@email.com</td>
                            <td>Apple</td>
                            <td>Enginering</td>
                            <td class="text-success">Empleado</td>
                            <td><a href="{{url('usuarios/actualizar/id')}}" class="btn btn-info btn-sm">Actualizar</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection