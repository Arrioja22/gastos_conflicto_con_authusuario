@extends('template')

@section('title','Usuarios')

@section('content')
<div class="row">
    <div class="col-lg">
        <a href="{{url('usuarios/nuevo')}}" class="btn shadow-sm btn-primary btn-block mb-4">
            <div class="row py-2 mx-auto">
                <div class="col">
                    <h2 class="text-white my-4">Crear usuarios</h2>
                </div>
                <div class="col-auto">
                    <div class="py-4">
                    <h1 class="mb-0"><i class="fa fa-user-circle"></i></h1>
                    </div>
                </div>
            </div>    
        </a>
    </div>
    <div class="col-lg">
        <a href="{{url('usuarios/actualizar')}}" class="btn shadow-sm btn-info btn-block mb-4">
        <div class="row py-2 mx-auto">
                <div class="col">
                    <h2 class="text-white my-4">Actualizar usuario</h2>
                </div>
                <div class="col-auto">
                    <div class="py-4">
                    <h1 class="mb-0"><i class="fa fa-history"></i></h1>
                    </div>
                </div>
            </div>    
        </a>
    </div>
    <div class="col-lg">
        <a href="{{url('usuarios/permisos')}}" class="btn shadow-sm btn-secondary bg-gray-900 btn-block mb-4">
        <div class="row py-2 mx-auto">
                <div class="col">
                    <h2 class="text-white my-4">Permisos</h2>
                </div>
                <div class="col-auto">
                    <div class="py-4">
                    <h1 class="mb-0"><i class="fa fa-user-lock"></i></h1>
                    </div>
                </div>
            </div>    
        </a>
    </div>
</div>
<div class="row">
    <div class="col-lg">
        <a href="{{url('usuarios/lista')}}" class="btn shadow-sm btn-warning btn-block mb-4">
        <div class="row py-2 mx-auto">
                <div class="col">
                    <h2 class="text-white my-4">Lista de usuario</h2>
                </div>
                <div class="col-auto">
                    <div class="py-4">
                    <h1 class="mb-0"><i class="fa fa-list"></i></h1>
                    </div>
                </div>
            </div>    
        </a>
    </div>
    <div class="col-lg">
        <a href="{{url('/usuarios/activos')}}" class="btn shadow-sm btn-success btn-block mb-4">
        <div class="row py-2 mx-auto">
                <div class="col">
                    <h2 class="text-white my-4">Usuarios activos</h2>
                </div>
                <div class="col-auto">
                    <div class="py-4">
                    <h1 class="mb-0"><i class="fa fa-power-off"></i></h1>
                    </div>
                </div>
            </div>    
        </a>
    </div>
    <div class="col-lg">
        <a href="#" class="btn btn-danger shadow-sm btn-block mb-4">
        <div class="row py-2 mx-auto">
                <div class="col">
                    <h2 class="text-white my-4">Eliminar usuarios</h2>
                </div>
                <div class="col-auto">
                    <div class="py-4">
                    <h1 class="mb-0"><i class="fa fa-times"></i></h1>
                    </div>
                </div>
            </div>    
        </a>
    </div>
</div>
@endsection