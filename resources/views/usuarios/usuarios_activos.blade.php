@extends('template')

@section('title','Usuarios activos')

@section('header')
<a href="{{url('/usuarios')}}" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left"></i> Regresar</a>
@endsection

@section('content')
<div class="row mt-4">
    <div class="col-lg mx-auto">
        <div class="card border-bottom-danger shadow">
            <div class="card-body table-responsive">
                <table id="usuarios" class="table align-items-center table-hover table-borderless table-flush">
                    <thead class="thead-light">
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Empresa</th>
                        <th>Departamento</th>
                        <th>Categoría</th>
                        <th>Estado</th>
                        <th></th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Isaac Newton</td>
                            <td>Google</td>
                            <td>Marketing</td>
                            <td class="text-success">Empleado</td>
                            <td><span class="badge badge-pill badge-success">Activo</span></td>
                            <td>
                                <a href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#modal-bloquear">Bloquear</a>
                            </td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Nickola Tesla</td>
                            <td>Tesla Motors</td>
                            <td>Engineering</td>
                            <td class="text-danger">Supervisor</td>
                            <td><span class="badge badge-pill badge-success">Activo</span></td>
                            <td><a href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#modal-bloquear">Bloquear</a></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Henry Ford</td>
                            <td>Ford</td>
                            <td>Ejecutivo</td>
                            <td class="text-danger">Supervisor</td>
                            <td><span class="badge badge-pill badge-danger">Bloqueado</span></td>
                            <td><a href="#" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal-activar">Activar</a></td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Linus Torvalds</td>
                            <td>Linux</td>
                            <td>Developing</td>
                            <td class="text-success">Empleado</td>
                            <td><span class="badge badge-pill badge-success">Activo</span></td>
                            <td><a href="#" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#modal-bloquear">Bloquear</a></td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>Steve Wozniak</td>
                            <td>Apple</td>
                            <td>Enginering</td>
                            <td class="text-success">Empleado</td>
                            <td><span class="badge badge-pill badge-danger">Bloqueado</span></td>
                            <td><a href="#" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal-activar">Activar</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- Modal Activar -->
<div class="modal fade" id="modal-activar" tabindex="-1" role="dialog" aria-labelledby="modal-activar" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
    <div class="modal-header bg-success">
    <h3 class="text-white mb-0">Activar usuario</h3>
    </div>
    <div class="modal-body">
        ¿está seguro que desea <strong class="text-success">ACTIVAR</strong> la cuenta de este usuario?
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
        <a href="#" class="btn btn-success">Aceptar</a>
    </div>
    </div>
</div>
</div>
<!-- Modal Bloquear -->
<div class="modal fade" id="modal-bloquear" tabindex="-1" role="dialog" aria-labelledby="modal-activar" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
    <div class="modal-header bg-danger">
    <h3 class="text-white mb-0">Activar usuario</h3>
    </div>
    <div class="modal-body">
        ¿está seguro que desea <strong class="text-danger">BLOQUEAR</strong> la cuenta de este usuario?
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
        <a href="#" class="btn btn-danger">Aceptar</a>
    </div>
    </div>
</div>
</div>

@endsection