@extends('template')

@section('title','Actualizar Usuario')

@section('header')
<a href="{{url('/usuarios/actualizar')}}" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left"></i> Regresar</a>
@endsection

@section('content')
<div class="row mt-4">
    <div class="col-lg-8 mx-auto">
        <div class="card shadow">
            <div class="card-header bg-info text-center">
                <h2 class="text-white mb-0">Actualizando: "nombre de usuario"</h2>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <form action="" method="post">
                            <div class="form-group">
                            <label class="text-info" for="nombre">Nombre:</label>
                                <input type="text" class="form-control" placeholder="Nombre" required>
                            </div>
                            <div class="form-group">
                            <label class="text-info" for="email">Correo electrónico:</label>
                                <input type="text" class="form-control" placeholder="Correo electrónico" required>
                            </div>
                            <div class="form-group">
                            <label class="text-info" for="banco">Información Bancaria:</label>
                                <input type="text" class="form-control" placeholder="Banco" required>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Cuenta" required>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Clabe" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                            <label class="text-info" for="departamento">Departamento:</label>
                                <select name="departamento" id="" class="form-control">
                                <option value="1">Sistemas</option>
                                <option value="2">Ventas</option>
                                </select>
                            </div>
                            <div class="form-group">
                            <label class="text-info" for="empresa">Empresa:</label>
                                <select name="empresa" id="" class="form-control" placeholder="Empresa" required>
                                    <option value="1">Google</option>
                                    <option value="2">Amazon</option>
                                </select>
                            </div>
                            <div  class="form-group">
                            <label class="text-info" for="categoríia">Categoría:</label>
                                <select name="categoria" id="" class="form-control" placeholder="Categoría" required>
                                    <option value="1">Supervisor</option>
                                    <option value="2">Empleado</option>
                                </select>
                            </div>
                            <div class="form-group">
                            <label class="text-info" for="password">Contraseña:</label>
                                <input type="password" class="form-control" placeholder="Contraseña" required>
                            </div>
                            <div class="form-group mb-0">
                                <button type="submit" class="btn btn-block btn-info">Guardar cambios</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection