<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('login');
});*/

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');--Este controlador no existe

Route::get('/', 'LoginUsuarioController@showLoginForm');
Route::post('login', 'LoginUsuarioController@login');
Route::post('/Salir', 'usuariosController@logout')->name('salir');




//---------Registro Usuario ------//
Route::resource('usuario', 'UsuarioController');

/*---------------- CREAR NUEVO USUARIO ------------- */
Route::resource('/usuarios/nuevo', 'UsuarioController');
Route::post('/usuarios/nuevo/crear', 'UsuarioController@store');
Route::post('usuario/crear', 'UsuarioController@store');


Route::get('password-reset', 'UsuarioController@requestPasswordReset')->name("RequestpasswordReset");
Route::post('password-reset/sendEmail', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('send_email');
Route::get('password-reset/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password_reset');

/* ----------------- SOLO VISTAS ------------------ */
Route::get('/recuperar', function(){
    return view('recuperacion');
});

Route::get('/inicio', function(){
    return view('index');
});

Route::get('/usuarios', function(){
    return view('usuarios.usuarios');
});

/*
Route::get('/usuarios/nuevo', function(){
    return view('usuarios.crear_usuario');
});
*/
Route::get('/usuarios/lista', function(){
    return view('usuarios.lista');
});

Route::get('/usuarios/actualizar',function(){
    return view('usuarios.actualizar_lista');
});

Route::get('/usuarios/actualizar/id', function(){
    return view('usuarios.actualizar');
});

Route::get('/usuarios/permisos', function(){
    return view('usuarios.permisos_lista');
});

Route::get('/usuarios/permisos/id', function(){
    return view('usuarios.permisos');
});

Route::get('/usuarios/activos', function(){
    return view('usuarios.usuarios_activos');
});

