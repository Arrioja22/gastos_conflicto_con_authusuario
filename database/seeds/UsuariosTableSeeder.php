<?php

use Illuminate\Database\Seeder;

class UsuariosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usuarios')->insert([
            'nombre' => 'Erick Ortiz',
            'email' => 'erick.95.10@hotmail.com',
            'banco' => 'Bbva Bancomer',
            'cuenta' => '018383',
            'clabe' => '098765432345678987',
            'departamento' => 76    ,
            'orden_servicio' => '098',
            'estatus' => 'success',
            'empresas_id' => 1,
            'categorias_id' => 1,
            'password' => '123456',
            ]);
        $this->command->info('La tabla se ha rellenado correctamente');
    }
}
