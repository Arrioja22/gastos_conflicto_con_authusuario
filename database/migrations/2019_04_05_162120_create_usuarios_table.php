<?php
/**
 * PHP Version 7.2.10
 * 
 * @category Migrations
 * @package  Database\Migrations
 * @author   Carlos <carlosflres095@gmail.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
 * @link     https://yoursite.com
 */
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Tabla Usuario
 * 
 * @category Controller
 * @package  Database\Migrations
 * @author   Carlos <carlosflres095@gmail.com>
 * @license  https://www.gnu.org/licenses/gpl-3.0.txt GNU/GPLv3
 * @link     https://yoursite.com
 */
class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'usuarios', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('nombre', 255);
                $table->string('email')->unique();
                $table->string('banco', 255);
                $table->string('cuenta', 255);
                $table->string('clabe', 255);
                $table->integer('departamento');
                $table->integer('orden_servicio');
                $table->string('estatus', 15);
                

                $table->unsignedBigInteger('empresas_id');
                $table->foreign('empresas_id')
                    ->references('id')->on('empresas')
                    ->onDelete('cascade');

                $table->integer('categorias_id');
                /*
                $table->unsignedBigInteger('categorias_id');
                $table->foreign('categorias_id')->references('id')->on('categorias');
                */
                $table->string('password');
                $table->rememberToken();
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
